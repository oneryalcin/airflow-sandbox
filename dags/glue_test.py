from airflow import DAG
from airflow.utils.dates import days_ago
from airflow.providers.amazon.aws.hooks.glue import AwsGlueJobHook


default_args = {
    'owner': 'oner',
    'start_date': days_ago(1)
}


glue_dag = DAG(
    dag_id='Glue_medicare_job',
    schedule_interval='@hourly',
    catchup=False,
    default_args=default_args,
    max_active_runs=1
)

glue_task = AwsGlueJobHook(
    s3_bucket='s3://oner-test-bucket/',
    script_location='s3://oner-test-bucket/source/gjob_medicare.py',
    num_of_dpus=1,
    region_name='eu-west-2',
    job_name='elasticsearch-company-ingest',
    
)

if __name__ == '__main__':
    print('Starting Task ')
    glue_task.list_jobs()
    print('Done job')
