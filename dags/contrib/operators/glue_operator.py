from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults
from airflow.providers.amazon.aws.hooks.glue import AwsGlueJobHook


class GlueOperator(BaseOperator):
    ui_color = '#a9c5fc'
    template_fields = ('job_name', 'script_arguments')

    @apply_defaults
    def __init__(self,
                 job_name,
                 script_arguments=None,
                 *args,
                 **kwargs):
        super(GlueOperator, self).__init__(*args, **kwargs)
        if script_arguments is None:
            script_arguments = {}
        self.job_name = job_name
        self.script_arguments = script_arguments

    def execute(self, context):
        glue_job = AwsGlueJobHook(job_name=self.job_name)
        glue_job_run = glue_job.initialize_job(script_arguments=self.script_arguments)
        glue_job_run = glue_job.job_completion(self.job_name, glue_job_run['JobRunId'])
        return glue_job_run['JobRunId']