from datetime import timedelta
from airflow import DAG
from airflow.providers.snowflake.hooks.snowflake import SnowflakeHook, SnowflakeConnection
from airflow.utils.dates import days_ago

args = {
    'owner': 'airflow',
}

with DAG(
        # dag_id=os.path.basename(__file__).replace(".py", ""),
        dag_id="elasticsearch_company_ingest_dag",
        default_args=args,
        schedule_interval='@daily',
        start_date=days_ago(1),
        dagrun_timeout=timedelta(minutes=120),
) as dag:

    snow = SnowflakeHook()

    snow

if __name__ == '__main__':
    hook = SnowflakeHook(snowflake_conn_id='snowflake_conn')

    bucket_uri = 's3://oner-test-bucket/temp-dir'
    table = 'customer'
    database = 'snowflake_sample_data'
    schema = 'TPCH_SF1'
    aws_key_id = 'AKIA4SWE5JKG5RI4S7FI'
    aws_secret_key = 'xoMdXdbP6afr6k9YKddN6TF0URzyTFa5gTy9zJQs'

    copy_sql = f"""
    COPY INTO
        {bucket_uri}/{table}/
    FROM
        {database}.{schema}.{table}
    CREDENTIALS = ( AWS_KEY_ID = '{aws_key_id}' AWS_SECRET_KEY = '{aws_secret_key}' )
    ENCRYPTION = ( TYPE = 'AWS_SSE_S3')
    FILE_FORMAT = ( TYPE = PARQUET COMPRESSION = SNAPPY)
    OVERWRITE = TRUE
    HEADER = TRUE;
    """
    hook.run(copy_sql)
    print(hook)
    #
    # conn_params = {
    #     "user": 'coursesoner',
    #     "password": 'Alcatel01!',
    #     "account": 'tk39170.eu-west-2.aws'
    # }
