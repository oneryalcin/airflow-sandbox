from typing import List
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults
from airflow.providers.snowflake.hooks.snowflake import SnowflakeHook
from airflow.providers.amazon.aws.hooks.s3 import S3Hook


class SnowflakeToS3Transfer(BaseOperator):
    ui_color = '#a9c5fc'
    template_fields = ('snowflake_conn_id', 'aws_conn_id', 'tables', 's3_path')

    @apply_defaults
    def __init__(self,
                 snowflake_conn_id: str,
                 aws_conn_id: str,
                 tables: List[str],
                 s3_path: str,
                 *args,
                 **kwargs):
        super(SnowflakeToS3Transfer, self).__init__(*args, **kwargs)
        self.snowflake_conn_id = snowflake_conn_id
        self.aws_conn_id = aws_conn_id
        self.tables = tables
        self.s3_path = s3_path[:-1] if s3_path.endswith('/') else s3_path
        self.kwargs = kwargs
        self.snowflake_hook = SnowflakeHook(snowflake_conn_id=snowflake_conn_id)
        self.s3_hook = S3Hook(aws_conn_id=aws_conn_id)

    @staticmethod
    def _batch_it(objects: list, chunk_size: int):
        """Helper function to yield a batch of list for a given chunk size

        :param objects:
        :param chunk_size:
        :return:
        """
        for i in range(0, len(objects), chunk_size):
            yield objects[i:i + chunk_size]

    def _empty_dir(self, s3_path, chunk_size=1000):
        """

        :param s3_path:
        :return:
        """

        # Since S3 is not a filesystem but an object store deleting all files under
        # a dir (since it is not a dir) is not as trivial as 'rm -rf dir/'
        # Therefore We'll do it in four stages:

        # 1) List all objects under the dir
        # 2) In case we have many objects get a batch of object keys
        # 3) delete objects in that batch
        # 4) Repeat 3 until we finish deleting all objects

        self.log.info('Removing all files under %s' % self.s3_path)
        bucket_name, keys = self.s3_hook.parse_s3_url(f"{s3_path}/")
        objects = self.s3_hook.list_keys(bucket_name, keys)

        if objects:
            self.log.info('There are %s objects to remove in batches of %s' % (len(objects), chunk_size))
            batches = self._batch_it(objects, chunk_size=chunk_size)
            for batch in batches:
                self.s3_hook.delete_objects(bucket=bucket_name, keys=batch)

        self.log.info('All %s files removed from %s' % (len(objects), self.s3_path))

    def _copy_from_snowflake_to_s3(self, table: str, snowflake_params: dict, aws_params: dict) -> None:
        """Copy a single table from snowflake to S3

        :param table: table name
        :param snowflake_params:
        :param aws_params:
        :return:
        """

        database = snowflake_params['database']
        schema = snowflake_params['schema']
        aws_key_id = aws_params['aws_key_id']
        aws_secret_key = aws_params['aws_secret_key']

        sql = f"""
            COPY INTO 
                {self.s3_path}/{table}/
            FROM 
                {database}.{schema}.{table}
            CREDENTIALS = ( AWS_KEY_ID = '{aws_key_id}' AWS_SECRET_KEY = '{aws_secret_key}' )
            ENCRYPTION = ( TYPE = 'AWS_SSE_S3')
            FILE_FORMAT = ( TYPE = PARQUET COMPRESSION = SNAPPY)
            OVERWRITE = TRUE
            HEADER = TRUE;
            """
        self.log.info('SQL command is: %s' % sql)
        con_params = self.snowflake_hook._get_conn_params()
        self.log.info('Snowflake connection params are: %s' % con_params)
        from snowflake import connector
        conn = connector.connect(**con_params)
        self.log.info('CONN %s' % conn)
        self.snowflake_hook.run(sql)

    def execute(self, context) -> None:
        """Copy Snowflake Tables to S3 as parquet files
        # TODO: Add Support to other formats, not just parquet

        # Game Plan
        # 1) If there are any files, remove all files under self.s3_path
        # 2) Copy all tables (one by one) to self.s3_path

        :param context:
        :return: None
        """

        # Remove all objects from S3 path before copying.
        self._empty_dir(s3_path=self.s3_path)

        # If database and schema are specified as kwargs consider them as priority
        # otherwise fall back to snowflake_connection defined in airflow connections
        snow_conn = self.snowflake_hook._get_conn_params()
        snowflake_params = {
            "database": self.kwargs.get('database') if self.kwargs.get('database') else snow_conn.get('database'),
            "schema": self.kwargs.get('schema') if self.kwargs.get('schema') else snow_conn.get('schema')
        }

        aws_credentials = self.s3_hook._get_credentials(region_name=None)[0].get_credentials()

        aws_params = {
            "aws_key_id": aws_credentials.access_key,
            "aws_secret_key": aws_credentials.secret_key
        }

        # Gracefully handle if table is a list of tables or a single table name
        if isinstance(self.tables, list):
            for table in self.tables:
                self.log.info('Copying table %s to s3 path %s' % (table, self.s3_path))
                self._copy_from_snowflake_to_s3(
                    table=table,
                    snowflake_params=snowflake_params,
                    aws_params=aws_params,
                )
        elif isinstance(self.tables, str):
            self.log.info('Copying table %s to s3 path %s' % (self.tables, self.s3_path))
            self._copy_from_snowflake_to_s3(
                table=self.tables,
                snowflake_params=snowflake_params,
                aws_params=aws_params
            )
        else:
            raise TypeError('`tables` should be either a list or a string')

        self.log.info('Copying tables: %s is completed successfully' % self.tables)
