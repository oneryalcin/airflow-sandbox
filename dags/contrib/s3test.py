#
# from airflow.providers.amazon.aws.hooks.s3 import S3Hook
#
#
# def chunk_it(lst, n):
#     for i in range(0, len(lst), n):
#         yield lst[i:i + n]
#
#
# if __name__ == '__main__':
#
#     # Since S3 is not a filesystem but a object store deleting all files under a dir (it's actually not a dir)
#     # is not as trivial as 'rm -rf dir/' We'll do it in four stages:
#     # 1) List all objects under the dir
#     # 2) In case we have many objects get a batch of object keys
#     # 3) delete objects in that batch
#     # 4) Repeat 3 until we finish deleting all objects
#     s3_hook = S3Hook(aws_conn_id='aws_default')
#     bucket_name, keys = s3_hook.parse_s3_url('s3://oner-test-bucket/temp-dir/customer')
#     objects = s3_hook.list_keys(bucket_name, keys)
#
#     if objects:
#         batches = chunk_it(objects, 1000)
#         for batch in batches:
#             s3_hook.delete_objects(bucket=bucket_name, keys=batch)
#
#     print(s3_hook)
