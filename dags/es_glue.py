from airflow import DAG
from airflow.utils.dates import days_ago
from airflow.providers.amazon.aws.hooks.glue import AwsGlueJobHook
# from contrib.operators.glue_operator import GlueOperator
from airflow.models import Variable
from datetime import datetime


# elasticsearch_companies_ingest = GlueOperator(
#         task_id='elasticsearch_companies_ingest',
#         job_name="Ingest Vault Oner",
#         script_arguments={
#             "--dbpassword": "UVhja3pzcE02cHY4Y1dyMkt5REhTUkRCRWhFVnZIeno==",
#             "--esport": 443,
#             "--dbhost": "snp2.ctkrmr272uks.eu-west-1.rds.amazonaws.com",
#             "--dbname": "postgres",
#             "--sentrytarget": "http://1db9069746dc40f486fe9da196b704e7@185.202.172.152:9000/2",
#             "--dbuser": "api_server",
#             "--es-access-key": "AKIA4PZOOR2SUHMZYMUD",
#             "--esindex": "company-test-index",
#             "--es-secret-access-key": "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",
#             "--dbport": 5432,
#             "--esdomain": "search-advent-dev-es2-ej3ipkbnqhlguqpgovumjlrqpu.eu-west-1.es.amazonaws.com"
#         }
#     )


if __name__ == '__main__':
    print('Starting Task ')
    # glue_task = AwsGlueJobHook(
    #     num_of_dpus=1,
    #     region_name='eu-west-2',
    #     job_name='elasticsearch-company-ingest')
    #
    # job = glue_task.initialize_job(
    #     script_arguments={
    #         "--dbpassword": "UVhja3pzcE02cHY4Y1dyMkt5REhTUkRCRWhFVnZIeno==",
    #         "--esport": "443",
    #         "--dbhost": "snp2.ctkrmr272uks.eu-west-1.rds.amazonaws.com",
    #         "--dbname": "postgres",
    #         "--sentrytarget": "http://1db9069746dc40f486fe9da196b704e7@185.202.172.152:9000/2",
    #         "--dbuser": "api_server",
    #         "--es-access-key": "AKIA4PZOOR2SUHMZYMUD",
    #         "--esindex": "company-test-index",
    #         "--es-secret-access-key": "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",
    #         "--dbport": "5432",
    #         "--esdomain": "search-advent-dev-es2-ej3ipkbnqhlguqpgovumjlrqpu.eu-west-1.es.amazonaws.com"
    #     }
    # )
    # print(job)

    glue_task = AwsGlueJobHook(
        num_of_dpus=1,
        region_name='eu-west-2',
        job_name='elasticsearch-people-ingest')

    job = glue_task.initialize_job(
        script_arguments={
            # "--legacy_rel_db_password": "UVhja3pzcE02cHY4Y1dyMkt5REhTUkRCRWhFVnZIeno==",
            "--dbpassword": Variable.get('legacy_rel_db_password'),
            "--esport": "443",
            "--dbhost": Variable.get('legacy_rel_db_host'),
            "--dbname": Variable.get('legacy_rel_db_name'),
            "--sentrytarget": Variable.get('sentry_gluejobs_dsn'),
            "--dbuser": Variable.get('legacy_rel_db_username'),
            "--es-access-key": Variable.get("es_access_key"),
            "--esindex": f"people-index-{datetime.now().strftime(format='%Y-%m-%d_%H-%M-%S')}",
            # "--esindexdev": "people-airflow-index-{{ ds }}",
            "--es-secret-access-key": Variable.get("es_secret_access_key"),
            "--dbport": "5432",
            # "--esdomain": Variable.get("esdomain"),
            "--esdomain": "search-advent-dev-es2-ej3ipkbnqhlguqpgovumjlrqpu.eu-west-1.es.amazonaws.com",
            "--esalias": "oner",
        }
    )
    print(job)
    # glue_task.list_jobs()
    print('Done job')
