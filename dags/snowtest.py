from datetime import timedelta
from airflow import DAG
from contrib.snowflaketos3 import SnowflakeToS3Transfer
from airflow.utils.dates import days_ago

args = {
    'owner': 'airflow',
}

with DAG(
        dag_id="snowflake_to_s3",
        default_args=args,
        schedule_interval='@daily',
        start_date=days_ago(2),
        dagrun_timeout=timedelta(minutes=120),
) as dag:

    snowflake_to_s3 = SnowflakeToS3Transfer(
        task_id='snowflake_to_s3',
        snowflake_conn_id='snowflake_conn',
        aws_conn_id='aws_default',
        tables=['CUSTOMER'],
        # database='snowflake_sample_data',
        # schema='TPCH_SF1',
        s3_path='s3://oner-test-bucket/temp-dir'
    )

    snowflake_to_s3
