from airflow import DAG
from airflow.utils.dates import days_ago
from airflow.operators.python_operator import PythonOperator
from airflow.contrib.hooks.gcs_hook import GoogleCloudStorageHook

default_args = {
    'owner': 'oner',
    'start_date': days_ago(1)
}


dag = DAG(
    dag_id='testing_dag',
    schedule_interval='@hourly',
    catchup=False,
    default_args=default_args,
    max_active_runs=1
)


def list_objects(bucket=None, **kwargs):
    hook = GoogleCloudStorageHook(google_cloud_storage_conn_id='google_cloud_default')
    storage_objects = hook.list(bucket=bucket)
    print(storage_objects)

    # kwargs['ti'].xcom_push(value=storage_objects)
    return storage_objects


def move_objects(source_bucket=None, destination_bucket=None, **kwargs):

    storage_objects = kwargs['ti'].xcom_pull(task_ids='list_files')
    hook = GoogleCloudStorageHook(google_cloud_storage_conn_id='google_cloud_default')

    for obj in storage_objects:
        hook.copy(source_bucket=source_bucket, source_object=obj, destination_bucket=destination_bucket)
        hook.delete(bucket=source_bucket, object=obj)


list_files = PythonOperator(
    task_id='list_files',
    python_callable=list_objects,
    op_kwargs={'bucket': 'logistics-landing-bucket-oner'},
    dag=dag,
    # provide_context=True
)


move_files = PythonOperator(
    task_id='move_files',
    python_callable=move_objects,
    op_kwargs={'source_bucket': 'udacity-oner'}
)

if __name__ == '__main__':
    print('starting debug')
    list_files.run(start_date=days_ago(1), end_date=days_ago(0))
    print('ending debug')