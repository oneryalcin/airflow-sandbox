import os
from airflow import DAG
from airflow.utils.dates import days_ago
from airflow.providers.amazon.aws.hooks.glue import AwsGlueJobHook
from contrib.operators.glue_operator import GlueOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.models import Variable
from datetime import datetime, timedelta


args = {
    'owner': 'airflow',
}

with DAG(
        # dag_id=os.path.basename(__file__).replace(".py", ""),
        dag_id="elasticsearch_company_ingest_dag",
        default_args=args,
        schedule_interval='@daily',
        start_date=days_ago(1),
        dagrun_timeout=timedelta(minutes=120),
) as dag:

    elasticsearch_companies_ingest = GlueOperator(
        task_id="ElasticsearchCompanyIngestionGlueJob",
        job_name="elasticsearch-company-ingest",
        num_of_dpus=16,
        script_arguments={
            "--dbpassword": Variable.get('legacy_rel_db_password'),
            "--esport": "443",
            "--dbhost": Variable.get('legacy_rel_db_host'),
            "--dbname": Variable.get('legacy_rel_db_name'),
            "--sentrytarget": Variable.get('sentry_gluejobs_dsn'),
            "--dbuser": Variable.get('legacy_rel_db_username'),
            "--es-access-key": Variable.get("es_access_key"),
            "--esindex": "companies-index-%s-{{ ds }}" % datetime.now().strftime(format='%Y-%m-%d_%H-%M-%S'),
            "--es-secret-access-key": Variable.get("es_secret_access_key"),
            "--dbport": "5432",
            # "--esdomain": Variable.get("esdomain"),
            "--esdomain": "search-advent-dev-es2-ej3ipkbnqhlguqpgovumjlrqpu.eu-west-1.es.amazonaws.com",
            "--esalias": "companies_search",
        }
    )

    # there is currently single task, but once we are in new DB This task will have dependency on concordance
    # task being complete.
    elasticsearch_companies_ingest

# if __name__ == '__main__':
#     print('Starting Task ')
#
#     glue_task = AwsGlueJobHook(
#         num_of_dpus=1,
#         region_name='eu-west-2',
#         job_name='elasticsearch-company-ingest')
#
#     job = glue_task.initialize_job(
#         script_arguments={
#             "--dbpassword": Variable.get('legacy_rel_db_password'),
#             "--esport": "443",
#             "--dbhost": Variable.get('legacy_rel_db_host'),
#             "--dbname": Variable.get('legacy_rel_db_name'),
#             "--sentrytarget": Variable.get('sentry_gluejobs_dsn'),
#             "--dbuser": Variable.get('legacy_rel_db_username'),
#             "--es-access-key": Variable.get("es_access_key"),
#             "--esindex": f"companies-index-{datetime.now().strftime(format='%Y-%m-%d_%H-%M-%S')}",
#             "--es-secret-access-key": Variable.get("es_secret_access_key"),
#             "--dbport": "5432",
#             # "--esdomain": Variable.get("esdomain"),
#             "--esdomain": "search-advent-dev-es2-ej3ipkbnqhlguqpgovumjlrqpu.eu-west-1.es.amazonaws.com",
#             "--esalias": "oner-companies",
#         }
#     )
#     print(job)
#     # glue_task.list_jobs()
#     print('Done job')