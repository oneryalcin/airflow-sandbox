from airflow import DAG
from airflow.utils.dates import days_ago
from airflow.operators.python_operator import PythonOperator
from airflow.contrib.hooks.gcs_hook import GoogleCloudStorageHook
from airflow.contrib.operators.gcs_to_bq import GoogleCloudStorageToBigQueryOperator
from airflow.contrib.operators.bigquery_operator import BigQueryOperator


default_args = {
    'owner': 'oner',
    'start_date': days_ago(1)
}


def list_objects(bucket=None):
    hook = GoogleCloudStorageHook(google_cloud_storage_conn_id='google_cloud_default')
    storage_objects = hook.list(bucket=bucket)

    return storage_objects


def move_objects(source_bucket=None, destination_bucket=None, **kwargs):

    storage_objects = kwargs['ti'].xcom_pull(task_ids='list_files')
    hook = GoogleCloudStorageHook(google_cloud_storage_conn_id='google_cloud_default')

    for obj in storage_objects:
        hook.copy(source_bucket=source_bucket, source_object=obj, destination_bucket=destination_bucket)
        hook.delete(bucket=source_bucket, object=obj)


with DAG(
    dag_id='bigquery_data_load',
    schedule_interval='@hourly',
    catchup=False,
    default_args=default_args,
    max_active_runs=1,
) as dag:

    list_files = PythonOperator(
        task_id='list_files',
        python_callable=list_objects,
        op_kwargs={'bucket': 'logistics-landing-bucket-oner'}
    )

    move_files = PythonOperator(
        task_id='move_files',
        python_callable=move_objects,
        op_kwargs={
            'source_bucket': 'logistics-landing-bucket-oner',
            'destination_bucket': 'logistics-backup-bucket-oner'
        },
        provide_context=True
    )

    load_data = GoogleCloudStorageToBigQueryOperator(
        task_id='load_data',
        bucket='logistics-landing-bucket-oner',
        source_objects=['*'],
        source_format='CSV',
        skip_leading_rows=1,
        field_delimiter=',',
        destination_project_dataset_table='udacity-oner.vehicle_analytics.history',
        create_disposition='CREATE_IF_NEEDED',
        write_disposition='WRITE_APPEND',
        bigquery_conn_id='google_cloud_default',
        google_cloud_storage_conn_id='google_cloud_default'
    )

    query = """
    SELECT * except (rank)
    FROM (
        SELECT 
            *,
            ROW_NUMBER() OVER (
                PARTITION BY vehicle_id ORDER BY DATETIME(date, TIME(hour, minute, 0)) DESC 
            ) as rank
        FROM 
            `udacity-oner.vehicle_analytics.history` as latest
    )
    WHERE 
        rank = 1;
    """

    create_table = BigQueryOperator(
        task_id='create_table',
        sql=query,
        destination_dataset_table='udacity-oner.vehicle_analytics.latest',
        write_disposition='WRITE_TRUNCATE',
        create_disposition='CREATE_IF_NEEDED',
        use_legacy_sql=False,
        location='us-central1',
        bigquery_conn_id='google_cloud_default'
    )

list_files >> load_data >> create_table >> move_files
